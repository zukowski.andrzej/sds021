#pragma once

#include <string>
#include <getopt.h>
#include <sds021/Protocol.hh>
#include "scenarios/Scenario.hh"

/**
 * @brief Wrapper class for getopt call
 */
class Options
{
public:
  /**
   * @brief Constructs Option objects
   */
  Options();

  /**
   * @brief Parses command line arguments
   *
   * @param argc number of string pointed by argv
   * @param argv list of strings representing command line arguments
   *
   * @return Scenario object representing one of several modes that 
   *         sds binary can run
   */
  Scenario::Ptr parse( int argc, char *argv[]);

private:
  static const option      mOptions[];
  Protocol::DevicePtr      mDevice;
  Protocol::DeviceIDPtr    mDeviceID;
  Scenario::Ptr            mScenario;

  void checkScenario( const Protocol::DevicePtr&   aDevice   );
  void checkScenario( const Protocol::DeviceIDPtr& aDeviceID );
  void checkScenario( const Scenario::Ptr&         aScenario );
  void forceScenario( const Scenario::Ptr&         aScenario );
};
