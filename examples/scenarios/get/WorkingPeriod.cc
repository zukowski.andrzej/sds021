#include <Factory.hh>
#include <Requests/WorkingPeriod.hh>
#include <Responses/WorkingPeriod.hh>

#include "WorkingPeriod.hh"

Scenario::Get::WorkingPeriod::WorkingPeriod()
{
}

void Scenario::Get::WorkingPeriod::operator()()
{
  Factory::Ptr  lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::WorkingPeriod ) );

  Request::Ptr  lRequest( new Request::WorkingPeriod<>( *mDeviceID ));
  lRequest->send( *mSerial );

  Response::Ptr lResponse( lFactory->receive( *mSerial ));
  if( lResponse )
    lResponse->print();
}
