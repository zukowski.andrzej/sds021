#pragma once

#include <scenarios/Scenario.hh>

namespace Scenario
{
namespace Get
{

/**
 * @brief Get Reporting Mode scenario
 */
class Reporting: public Scenario
{
public:
  /*
   * @brief Constructs Get Reporting Mode scenario
   */
  Reporting();

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();
private:
};

} // namespace Get
} // namespace Scenario
