#include <Factory.hh>
#include <Responses/Query.hh>

#include "Query.hh"

Scenario::Get::Query::Query( uint32_t aCount )
  : mCount( aCount )
{
}

void Scenario::Get::Query::operator()()
{
  Factory::Ptr lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::Query ) );

  for( uint32_t lCount = 0; lCount < mCount || !mCount  ;lCount++)
  {
    Response::Ptr lResponse( lFactory->receive( *mSerial ));
    if( lResponse )
      lResponse->print();
  }
}
