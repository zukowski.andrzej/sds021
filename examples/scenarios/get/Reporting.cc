#include <Factory.hh>
#include <Requests/Reporting.hh>
#include <Responses/Reporting.hh>

#include "Reporting.hh"

Scenario::Get::Reporting::Reporting()
{
}

void Scenario::Get::Reporting::operator()()
{
  Factory::Ptr  lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::Reporting) );

  Request::Ptr  lRequest( new Request::Reporting<>( *mDeviceID ));
  lRequest->send( *mSerial );

  Response::Ptr lResponse( lFactory->receive( *mSerial ));
  if( lResponse )
    lResponse->print();
}
