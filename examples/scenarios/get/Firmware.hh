#pragma once

#include <scenarios/Scenario.hh>

namespace Scenario
{
namespace Get
{

/**
 * @brief Get Firmware scenario
 */
class Firmware : public Scenario
{
public:
  /*
   * @brief Constructs Get Firmware scenario
   */
  Firmware();

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();
private:
};

} // namespace Get
} // namespace Scenario
