#pragma once

#include <scenarios/Scenario.hh>

namespace Scenario
{
namespace Get
{

/**
 * @brief Query scenario
 */
class Query : public Scenario
{
public:
  /*
   * @brief Constructs Query Scenario
   *
   * @param aCount Number of Query responses scenario accepts
   *               0 - continuous mode
   */
  Query( uint32_t aCount );

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();
private:
  uint32_t mCount;
};

} // namespace Get
} // namespace Scenario
