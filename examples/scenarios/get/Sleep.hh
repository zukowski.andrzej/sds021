#pragma once

#include <scenarios/Scenario.hh>

namespace Scenario
{
namespace Get
{

/**
 * @brief Get Sleep Mode scenario
 */
class Sleep : public Scenario
{
public:
  /*
   * @brief Constructs Get Sleep Mode scenario
   */
  Sleep();

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();
private:
};

} // namespace Get
} // namespace Scenario
