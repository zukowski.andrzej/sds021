#pragma once

#include <scenarios/Scenario.hh>

namespace Scenario
{
namespace Get
{

/**
 * @brief Get Working Period scenario
 */
class WorkingPeriod : public Scenario
{
public:
  /*
   * @brief Constructs Get Working Period scenario
   */
  WorkingPeriod();

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();
private:
};

} // namespace Get
} // namespace Scenario
