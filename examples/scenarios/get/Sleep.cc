#include <Factory.hh>
#include <Requests/Sleep.hh>
#include <Responses/Sleep.hh>

#include "Sleep.hh"

Scenario::Get::Sleep::Sleep()
{
}

void Scenario::Get::Sleep::operator()()
{
  Factory::Ptr  lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::Sleep ) );

  Request::Ptr  lRequest( new Request::Sleep<Request::Action::SET>( *mDeviceID ));
  lRequest->send( *mSerial );

  Response::Ptr lResponse( lFactory->receive( *mSerial ));
  if( lResponse )
    lResponse->print();
}
