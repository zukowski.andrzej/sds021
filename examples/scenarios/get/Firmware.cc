#include <Factory.hh>
#include <Requests/Firmware.hh>
#include <Responses/Firmware.hh>

#include "Firmware.hh"

Scenario::Get::Firmware::Firmware()
{
}

void Scenario::Get::Firmware::operator()()
{
  Factory::Ptr  lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::Firmware ) );

  Request::Ptr  lRequest( new Request::Firmware( *mDeviceID ) );
  lRequest->send( *mSerial );

  lFactory->receive( *mSerial )->print();
}
