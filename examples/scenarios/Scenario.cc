#include <iostream>
#include "Scenario.hh"

Scenario::Scenario::Scenario()
  : mSerial()
  , mDevice( new Protocol::Device( "/dev/ttyUSB0" ) )
  , mDeviceID( new Protocol::DeviceID( 0xffff ) )
{
}

void Scenario::Scenario::set( const Protocol::DevicePtr& aDevice )
{
  mDevice = aDevice;
}

void Scenario::Scenario::set( const Protocol::DeviceIDPtr& aDeviceID )
{
  mDeviceID = aDeviceID;
}

void Scenario::Scenario::connect()
{
  mSerial.reset( new SerialPort( *mDevice ));
  mSerial->Open
    ( SerialPort::BAUD_9600
    , SerialPort::CHAR_SIZE_8
    , SerialPort::PARITY_NONE );
}

