#pragma once

#include <memory>
#include <string>
#include <SerialPort.h>
#include <sds021/Protocol.hh>

namespace Scenario
{

/**
 * @brief Base class for all SDS021 communication scenarios
 */
class Scenario
{
public:
  /// Shared pointer for SerialPort class
  typedef std::shared_ptr< SerialPort > SerialPortPtr;

  /**
   * @brief Constructs scenario object
   *
   * Scenario object needs Device and DeviceID object to properly
   * communicate with SDS021. If none of them are given then default
   * values are in place ("/dev/ttyUSB0". 0xFFFF).
   */
  Scenario();

  /**
   * @brief Sets Device name that under which dust sensor is visible in OS
   *
   * @param aDevice Device name under which dust sensor is visible in OS
   */
  void set( const Protocol::DevicePtr&   aDevice   );

  /**
   * @brief Sets Device Identifier
   *
   * @param aDeviceID Device identifier
   */
  void set( const Protocol::DeviceIDPtr& aDeviceID );

  /**
   * @brief Connects to Serial Port
   */
  virtual void connect();

  /**
   * @brief Function operator that executes scenario
   */
  virtual void operator()() = 0;
protected:

  SerialPortPtr         mSerial;
  Protocol::DevicePtr   mDevice;
  Protocol::DeviceIDPtr mDeviceID;
};

/// Shared pointer for Scenario class
typedef std::shared_ptr< Scenario > Ptr;

} // namespace scenario
