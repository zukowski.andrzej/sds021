#pragma once

#include "Scenario.hh"

namespace Scenario
{

/**
 * @brief Help scenario printing sds usage
 */
class Help : public Scenario
{
public:
  /*
   * @brief Constructs Help scenario
   */
  Help();

  /**
   * @brief Connects to Serial Port
   */
  virtual void connect();

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();
private:
};

} // namespace scenario
