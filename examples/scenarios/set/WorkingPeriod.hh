#pragma once

#include <sds021/Protocol.hh>
#include <scenarios/Scenario.hh>

namespace Scenario
{
namespace Set
{

/**
 * @brief Set Working Period scenario
 */
class WorkingPeriod : public Scenario
{
public:
  /**
   * @brief Constructs Set Working Period scenario
   *
   * @param aWorkingPeriod - Sets working period in minutes 1-30
   *                         0 - continuous mode
   */
  WorkingPeriod( Protocol::Byte aWorkingPeriod );

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();
private:
  Protocol::Byte mWorkingPeriod;
};

} // namespace Set
} // namespace Scenario
