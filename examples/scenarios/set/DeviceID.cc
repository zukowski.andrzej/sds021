#include <Factory.hh>
#include <Requests/DeviceID.hh>
#include <Responses/DeviceID.hh>

#include "DeviceID.hh"

Scenario::Set::DeviceID::DeviceID( Protocol::DeviceID aNewDeviceID )
  : mNewDeviceID( aNewDeviceID )
{
}

void Scenario::Set::DeviceID::operator()()
{
  Factory::Ptr  lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::DeviceID ) );

  Request::Ptr  lRequest( new Request::DeviceID( *mDeviceID, mNewDeviceID ));
  lRequest->send( *mSerial );

  Response::Ptr lResponse( lFactory->receive( *mSerial ));
  if( lResponse )
    lResponse->print();
}
