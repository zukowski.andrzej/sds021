#pragma once

#include <scenarios/Scenario.hh>

namespace Scenario
{
namespace Set
{

/**
 * @brief Set Sleep Mode scenario
 */
class Sleep : public Scenario
{
public:
  /**
   * @brief Constructs Set Sleep Mode scenario
   *
   * @param aSleepMode Either WORK or SLEEP mode
   */
  Sleep( Request::SleepMode aSleepMode );

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();
private:
  Request::SleepMode mSleepMode;
};

} // namespace Set
} // namespace Scenario
