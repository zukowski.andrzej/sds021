#pragma once

#include <scenarios/Scenario.hh>

namespace Scenario
{
namespace Set
{

/**
 * @brief Set Device Identifier scenario
 */
class DeviceID : public Scenario
{
public:
  /**
   * @brief Constructs Set Device Identifier object
   *
   * @param aNewDeviceID New Device identifier
   */
  DeviceID( Protocol::DeviceID aNewDeviceID );

  /**
   * @brief Function operator that executes scenario
   */
  void operator()();

private:
  Protocol::DeviceID mNewDeviceID;
};

} // namespace Set
} // namespace Scenario
