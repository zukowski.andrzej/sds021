#include <Factory.hh>
#include <Requests/Sleep.hh>
#include <Responses/Sleep.hh>

#include "Sleep.hh"

Scenario::Set::Sleep::Sleep( Request::SleepMode aSleepMode )
  : mSleepMode( aSleepMode )
{
}

void Scenario::Set::Sleep::operator()()
{
  Factory::Ptr  lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::Sleep ) );

  Request::Ptr  lRequest(
    mSleepMode == Request::SleepMode::SLEEP
    ? Request::Ptr( new Request::Sleep< Request::Action::SET, Request::SleepMode::SLEEP >( *mDeviceID ))
    : Request::Ptr( new Request::Sleep< Request::Action::SET, Request::SleepMode::WORK  >( *mDeviceID )));
  lRequest->send( *mSerial );

  Response::Ptr lResponse( lFactory->receive( *mSerial ));
  if( lResponse )
    lResponse->print();
}
