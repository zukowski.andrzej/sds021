#include <Factory.hh>
#include <Requests/Reporting.hh>
#include <Responses/Reporting.hh>

#include "Reporting.hh"

Scenario::Set::Reporting::Reporting( Request::ReportingMode aReportingMode )
  : mReportingMode( aReportingMode )
{
}

void Scenario::Set::Reporting::operator()()
{
  Factory::Ptr  lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::Reporting) );

  Request::Ptr  lRequest(
    mReportingMode == Request::ReportingMode::ACTIVE
    ? Request::Ptr( new Request::Reporting< Request::Action::SET, Request::ReportingMode::ACTIVE >( *mDeviceID ))
    : Request::Ptr( new Request::Reporting< Request::Action::SET, Request::ReportingMode::QUERY  >( *mDeviceID )));
  lRequest->send( *mSerial );

  Response::Ptr lResponse( lFactory->receive( *mSerial ));
  if( lResponse )
    lResponse->print();
}
