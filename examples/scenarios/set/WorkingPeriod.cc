#include <Factory.hh>
#include <Requests/WorkingPeriod.hh>
#include <Responses/WorkingPeriod.hh>

#include "WorkingPeriod.hh"

Scenario::Set::WorkingPeriod::WorkingPeriod( Protocol::Byte aWorkingPeriod )
  : mWorkingPeriod( aWorkingPeriod )
{
}

void Scenario::Set::WorkingPeriod::operator()()
{
  Factory::Ptr  lFactory( new Factory );
  lFactory->add( Response::Ptr( new Response::WorkingPeriod ) );

  Request::Ptr  lRequest( new Request::WorkingPeriod<Request::Action::SET>( *mDeviceID, mWorkingPeriod ) );
  lRequest->send( *mSerial );

  Response::Ptr lResponse( lFactory->receive( *mSerial ));
  if( lResponse )
    lResponse->print();
}
