#pragma once

#include <scenarios/Scenario.hh>
#include <Requests/Base.hh>

namespace Scenario
{
namespace Set
{

/**
 * @brief Set Reporting Mode scenario
 */
class Reporting: public Scenario
{
public:
  /**
   * @brief Constructs Set Reporting Mode scenario
   *
   * @param aReportingMode either ACTIVE or QUERY reporting mode
   */
  Reporting( Request::ReportingMode aReportingMode );


  /**
   * @brief Function operator that executes scenario
   */
  void operator()();

private:
  Request::ReportingMode mReportingMode;
};

} // namespace Set
} // namespace Scenario
