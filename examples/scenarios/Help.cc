#include <iostream>

#include "Help.hh"

Scenario::Help::Help()
{
}

void Scenario::Help::connect()
{
}

void Scenario::Help::operator()()
{
  std::cout << "Usage: sds [--id ID] [--dev device] --getFirmware"                         << std::endl
            << "  or:  sds [--id ID] [--dev device] --getQuery <count>"                    << std::endl
            << "  or:  sds [--id ID] [--dev device] --getReportingMode"                    << std::endl
            << "  or:  sds [--id ID] [--dev device] --getSleep"                            << std::endl
            << "  or:  sds [--id ID] [--dev device] --getWorkingPeriod"                    << std::endl
            << "  or:  sds [--id ID] [--dev device] --setReportingMode < ACTIVE | QUERY >" << std::endl
            << "  or:  sds [--id ID] [--dev device] --setDeviceID"                         << std::endl
            << "  or:  sds [--id ID] [--dev device] --setSleep < SLEEP | WORK >"           << std::endl
            << "  or:  sds [--id ID] [--dev device] --setWorkingPeriod"                    << std::endl
            << "  or:  sds --help"                                                         << std::endl
            << std::endl
            << "Options:"                                                                  << std::endl
            << "--id      SDS device ID ( default 0xffff )"                                << std::endl
            << "--dev     Serial Port   ( default /dev/ttyUSB0 )"                          << std::endl;
}
