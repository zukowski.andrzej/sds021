#include <iostream>

#include "Options.hh"
#include "scenarios/Help.hh"
#include "scenarios/get/Firmware.hh"
#include "scenarios/get/Query.hh"
#include "scenarios/get/Reporting.hh"
#include "scenarios/get/Sleep.hh"
#include "scenarios/get/WorkingPeriod.hh"
#include "scenarios/set/Reporting.hh"
#include "scenarios/set/DeviceID.hh"
#include "scenarios/set/Sleep.hh"
#include "scenarios/set/WorkingPeriod.hh"

const option Options::mOptions[] = {
    {"dev",              required_argument, nullptr, 'd'},
    {"id",               required_argument, nullptr, 'x'},
    {"getQuery",         required_argument, nullptr, 'n'},
    {"getFirmware",      no_argument,       nullptr, 'f'},
    {"getReportingMode", no_argument,       nullptr, 'r'},
    {"getSleep",         no_argument,       nullptr, 's'},
    {"getWorkingPeriod", no_argument,       nullptr, 'w'},
    {"setReporting",     required_argument, nullptr, 'm'},
    {"setDeviceID",      required_argument, nullptr, 'i'},
    {"setSleep",         required_argument, nullptr, 'l'},
    {"setWorkingPeriod", required_argument, nullptr, 'p'},
    {"help",             no_argument,       nullptr, 'h'},
    {nullptr,            0,                 nullptr, 0  }
  };

Options::Options()
  : mDevice()
  , mDeviceID()
  , mScenario()
{
}

Scenario::Ptr Options::parse( int argc, char *argv[])
{
  bool lContinue(true);

  while ( lContinue )
  {
    const auto opt = getopt_long(argc, argv, "", mOptions, nullptr );

    if (-1 == opt)
    {
      break;
    }

    switch (opt)
    {
      case 'd':
        checkScenario( Protocol::DevicePtr( new Protocol::Device( optarg )));
        break;
      case 'x':
        checkScenario( Protocol::DeviceIDPtr( new Protocol::DeviceID( atoi(optarg) )));
        break;
      case 'n':
        checkScenario( Scenario::Ptr( new Scenario::Get::Query( atoi(optarg)  )));
        break;
      case 'f':
        checkScenario( Scenario::Ptr( new Scenario::Get::Firmware( )));
        break;
      case 'r':
        checkScenario( Scenario::Ptr( new Scenario::Get::Reporting( )));
        break;
      case 's':
        checkScenario( Scenario::Ptr( new Scenario::Get::Sleep( )));
        break;
      case 'w':
        checkScenario( Scenario::Ptr( new Scenario::Get::WorkingPeriod( )));
        break;
      case 'm':
        {
          std::string lOptarg( optarg );
          lOptarg == "ACTIVE"
            ? checkScenario( Scenario::Ptr( new Scenario::Set::Reporting( Request::ReportingMode::ACTIVE )))
            : lOptarg == "QUERY"
              ? checkScenario( Scenario::Ptr( new Scenario::Set::Reporting( Request::ReportingMode::QUERY )))
              : forceScenario( Scenario::Ptr( new Scenario::Help()));
        }
        break;
      case 'i':
        checkScenario( Scenario::Ptr( new Scenario::Set::DeviceID( atoi( optarg ) )));
        break;
      case 'l':
        {
          std::string lOptarg( optarg );
          lOptarg == "SLEEP"
            ? checkScenario( Scenario::Ptr( new Scenario::Set::Sleep( Request::SleepMode::SLEEP )))
            : lOptarg == "WORK"
              ? checkScenario( Scenario::Ptr( new Scenario::Set::Sleep( Request::SleepMode::WORK )))
              : forceScenario( Scenario::Ptr( new Scenario::Help()));
        }
        break;
      case 'p':
        checkScenario( Scenario::Ptr( new Scenario::Set::WorkingPeriod( atoi( optarg ) )));
        break;
      case 'h':
      default:
        forceScenario( Scenario::Ptr( new Scenario::Help()));
        lContinue = false;
        break;
    }
  }

  if( !mScenario)
  {
    forceScenario( Scenario::Ptr( new Scenario::Help()));
  }
  else
  {
    if( mDevice )
      mScenario->set( mDevice );
    if( mDeviceID )
      mScenario->set( mDeviceID );
  }

  return mScenario;
}

void Options::checkScenario( const Protocol::DevicePtr& aDevice )
{
  if( !mDevice )
  {
    mDevice = aDevice;
  }
  else
  {
    mScenario.reset( new Scenario::Help() );
  }
}

void Options::checkScenario( const Protocol::DeviceIDPtr& aDeviceID )
{
  if( !mDeviceID )
  {
    mDeviceID = aDeviceID;
  }
  else
  {
    mScenario.reset( new Scenario::Help() );
  }
}

void Options::checkScenario( const Scenario::Ptr& aScenario )
{
  if( !mScenario )
  {
    mScenario = aScenario;
  }
  else
  {
    mScenario.reset( new Scenario::Help() );
  }
}

void Options::forceScenario( const Scenario::Ptr& aScenario )
{
  mScenario.reset( new Scenario::Help() );
}
