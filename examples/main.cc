#include <iostream>
#include <Options.hh>
#include <scenarios/Scenario.hh>

int main(int argc, char *argv[])
{
  Options lOptions;

  Scenario::Ptr lScenario( lOptions.parse( argc, argv ));

  try {
    lScenario->connect();
    (*lScenario)();
  }
  catch( const std::exception & lException )
  {
    std::cerr << lException.what() << std::endl;
    exit(-1);
  }

  return 0;
}

