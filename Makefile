BUILD_DIR=build

SRCS=sds021/Factory.cc \
     sds021/Protocol.cc \
     sds021/Requests/Base.cc \
     sds021/Requests/Firmware.cc \
     sds021/Responses/Base.cc \
     sds021/Responses/Firmware.cc \
     sds021/Responses/Query.cc \
     sds021/Responses/DeviceID.cc \
     sds021/Responses/Reporting.cc \
     sds021/Responses/Sleep.cc \
     sds021/Responses/WorkingPeriod.cc \
     examples/main.cc \
     examples/Options.cc \
     examples/scenarios/Help.cc \
     examples/scenarios/Scenario.cc \
     examples/scenarios/get/Firmware.cc \
     examples/scenarios/get/Query.cc \
     examples/scenarios/get/Reporting.cc \
     examples/scenarios/get/Sleep.cc \
     examples/scenarios/get/WorkingPeriod.cc \
     examples/scenarios/set/DeviceID.cc \
     examples/scenarios/set/Reporting.cc \
     examples/scenarios/set/Sleep.cc \
     examples/scenarios/set/WorkingPeriod.cc

OBJS=$(addprefix $(BUILD_DIR)/, $(SRCS:.cc=.o))

$(info $(OBJS))

PROGRAM=sds

all: $(OBJS)
	g++ -o $(BUILD_DIR)/$(PROGRAM) $(OBJS) -lserial -lpthread

$(BUILD_DIR)/%.o: %.cc
	mkdir -p `dirname $(BUILD_DIR)/$<`
	g++ -std=c++11 -I . -I examples -I sds021 -g -c $< -o $@

clean:
	rm -r $(BUILD_DIR)
