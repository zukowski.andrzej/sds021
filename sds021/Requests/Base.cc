#include <numeric>
#include "Base.hh"

Request::Base::Base( Protocol::DeviceID aDeviceID )
  : mDeviceID( aDeviceID )
{
}

void Request::Base::checksum( SerialPort::DataBuffer &aBuffer )
{
   aBuffer[17] =  std::accumulate( aBuffer.begin()+2, aBuffer.begin()+17, 0) & 0xff;
}
