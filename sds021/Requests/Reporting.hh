#pragma once

#include <SerialPort.h>
#include "Base.hh"

namespace Request
{

/**
 * @brief Request class that sets/gets reporting mode to dust meter
 *
 * @tparam ACTION Either GET or SET
 * @tparam MODE   Either ACTIVE or QUERY
 */
template< Request::Action ACTION = Request::Action::GET, Request::ReportingMode MODE = Request::ReportingMode::ACTIVE >
class Reporting : public Base
{
public:
  /**
   * @brief Constructs request for Reporting Mode configuration
   *
   * @param aDeviceID Current identifier value or 0xffff if request applies to any dust meter
   */
  Reporting( Protocol::DeviceID aDeviceID );

  /**
   * @brief Composes and sends request via SerialPort.
   *
   * @param aPort Serial Port on which SDS021 is accessible.
   */
  virtual void send( SerialPort & aPort ) const;
};


template< Request::Action ACTION, Request::ReportingMode MODE >
Reporting<ACTION, MODE>::Reporting( Protocol::DeviceID aDeviceID )
  : Base( aDeviceID )
{
}

template< Request::Action ACTION, Request::ReportingMode MODE >
void Reporting<ACTION, MODE>::send( SerialPort & aPort ) const
{
    SerialPort::DataBuffer lBuffer = { Protocol::Head
                                     , 0xB4
                                     , 0x02
                                     , static_cast< Protocol::Byte >( ACTION )
                                     , static_cast< Protocol::Byte >( MODE )
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , static_cast< Protocol::Byte >(  mDeviceID && 0xFF )
                                     , static_cast< Protocol::Byte >(( mDeviceID >> 8 ))
                                     , 0x00
                                     , Protocol::Tail
                                     };
    checksum( lBuffer );
    aPort.Write( lBuffer );
}

} // namespace Request
