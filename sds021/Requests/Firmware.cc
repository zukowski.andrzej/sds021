#include "Firmware.hh"

Request::Firmware::Firmware( Protocol::DeviceID aDeviceID )
  : Base( aDeviceID )
{
}

void Request::Firmware::send( SerialPort & aPort ) const
{
    SerialPort::DataBuffer lBuffer = { Protocol::Head
                                     , 0xB4
                                     , 0x07
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , static_cast< Protocol::Byte >(  mDeviceID && 0xFF )
                                     , static_cast< Protocol::Byte >(( mDeviceID >> 8 ))
                                     , 0x00
                                     , Protocol::Tail
                                     };
    checksum( lBuffer );
    aPort.Write( lBuffer );
}
