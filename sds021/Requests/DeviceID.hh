#pragma once

#include <SerialPort.h>
#include "Base.hh"

namespace Request
{

/**
 * @brief Request class that sets new DeviceID
 */
class DeviceID : public Base
{
public:
  /**
   * @brief Constructs request for DeviceID configuration
   *
   * @param aDeviceID    Current identifier value or 0xffff if request applies to any dust meter
   * @param aNewDeviceID New device identifer
   */
  DeviceID( Protocol::DeviceID aDeviceID, Protocol::DeviceID aNewDeviceID );

  /**
   * @brief Composes and sends request via SerialPort.
   *
   * @param aPort Serial Port on which SDS021 is accessible.
   */
  virtual void send( SerialPort & aPort  ) const;
private:
  Protocol::DeviceID mNewDeviceID;
};

DeviceID::DeviceID( Protocol::DeviceID aDeviceID, Protocol::DeviceID aNewDeviceID )
  : Base( aDeviceID )
  , mNewDeviceID( aNewDeviceID )
{
}

void DeviceID::send( SerialPort & aPort ) const
{
    SerialPort::DataBuffer lBuffer = { Protocol::Head
                                     , 0xB4
                                     , 0x05
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , static_cast< Protocol::Byte >(  mNewDeviceID && 0xFF )
                                     , static_cast< Protocol::Byte >(( mNewDeviceID >> 8 ))
                                     , static_cast< Protocol::Byte >(  mDeviceID && 0xFF )
                                     , static_cast< Protocol::Byte >(( mDeviceID >> 8 ))
                                     , 0x00
                                     , Protocol::Tail
                                     };
    checksum( lBuffer );
    aPort.Write( lBuffer );
}

} // namespace Request
