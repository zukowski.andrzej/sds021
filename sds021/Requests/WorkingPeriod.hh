#pragma once

#include <SerialPort.h>
#include "Base.hh"

namespace Request
{

/**
 * @brief Request class that sets/gets working period to dust meter
 *
 * @tparam ACTION Either GET or SET
 */
template< Request::Action ACTION = Request::Action::GET >
class WorkingPeriod : public Base
{
public:
   class UnsupportedPeriodValue : public std::runtime_error
   {
   public:
       UnsupportedPeriodValue( const std::string& aWhat ) :
           runtime_error(aWhat)
       {}
   };

  /**
   * @brief Constructs request for Working Period configuration
   *
   * @param aDeviceID Current identifier value or 0xffff if request applies to any dust meter
   * @param aPeriod   Working period 0    - continuous
   *                                 1-30 - every 1 - 30 minutes
   */
  WorkingPeriod( Protocol::DeviceID aDeviceID, Protocol::Byte aPeriod = 1);

  /**
   * @brief Composes and sends request via SerialPort.
   *
   * @param aPort Serial Port on which SDS021 is accessible.
   */
  virtual void send( SerialPort & aPort ) const;

private:
  Protocol::Byte mPeriod;
};

template< Request::Action ACTION >
WorkingPeriod< ACTION >::WorkingPeriod( Protocol::DeviceID aDeviceID, Protocol::Byte aPeriod )
  : Base( aDeviceID )
  , mPeriod( aPeriod )
{
  if( aPeriod > 30 )
  {
    std::string lError( "Unsupported period value: " );
    lError += aPeriod;

    throw UnsupportedPeriodValue( lError );
  }
}

template< Request::Action ACTION >
void WorkingPeriod< ACTION >::send( SerialPort & aPort ) const
{
  SerialPort::DataBuffer lBuffer = { Protocol::Head
                                   , 0xB4
                                   , 0x08
                                   , static_cast< Protocol::Byte >( ACTION )
                                   , mPeriod
                                   , 0x00
                                   , 0x00
                                   , 0x00
                                   , 0x00
                                   , 0x00
                                   , 0x00
                                   , 0x00
                                   , 0x00
                                   , 0x00
                                   , 0x00
                                   , static_cast< Protocol::Byte >(  mDeviceID && 0xFF )
                                   , static_cast< Protocol::Byte >(( mDeviceID >> 8 ))
                                   , 0x00
                                   , Protocol::Tail
                                   };
  checksum( lBuffer );
  aPort.Write( lBuffer );
}

} // namespace Request
