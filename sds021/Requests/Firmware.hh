#pragma once

#include <SerialPort.h>
#include "Base.hh"

namespace Request
{

/**
 * @brief Request class that reads firmware version from dust sensor
 */
class Firmware : public Base
{
public:
  /**
   * @brief Constructs request for Firmware query
   *
   * @param aDeviceID Current identifier value or 0xffff if request applies to any dust meter
   */
  Firmware( Protocol::DeviceID aDeviceID );

  /**
   * @brief Composes and sends request via SerialPort.
   *
   * @param aPort Serial Port on which SDS021 is accessible.
   */
  virtual void send( SerialPort & aPort ) const;
private:
};

} // namespace Request
