#pragma once

#include <SerialPort.h>
#include "Base.hh"

namespace Request
{

/**
 * @brief Request class that sets/gets sleep mode to dust sensor
 *
 * @tparam ACTION Either GET or SET
 * @tparam MODE   Either SLEEP or WORK
 */
template< Request::Action ACTION = Request::Action::GET, Request::SleepMode MODE = Request::SleepMode::SLEEP >
class Sleep : public Base
{
public:
  /**
   * @brief Constructs request for Sleep Mode configuration
   *
   * @param aDeviceID Current identifier value or 0xffff if request applies to any dust meter
   */
  Sleep( Protocol::DeviceID aDeviceID );

  /**
   * @brief Composes and sends request via SerialPort.
   *
   * @param aPort Serial Port on which SDS021 is accessible.
   */
  virtual void send( SerialPort & aPort  ) const;
};

template< Request::Action ACTION, Request::SleepMode MODE  >
Sleep< ACTION, MODE >::Sleep( Protocol::DeviceID aDeviceID )
  : Base( aDeviceID )
{
}

template< Request::Action ACTION, Request::SleepMode MODE  >
void Sleep< ACTION, MODE >::send( SerialPort & aPort ) const
{
    SerialPort::DataBuffer lBuffer = { Protocol::Head
                                     , 0xB4
                                     , 0x06
                                     , static_cast< Protocol::Byte >( ACTION )
                                     , static_cast< Protocol::Byte >( MODE )
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , 0x00
                                     , static_cast< Protocol::Byte >(  mDeviceID && 0xFF )
                                     , static_cast< Protocol::Byte >(( mDeviceID >> 8 ))
                                     , 0x00
                                     , Protocol::Tail
                                     };
    checksum( lBuffer );
    aPort.Write( lBuffer );
}

} // namespace Request
