#pragma once

#include <memory>
#include <SerialPort.h>

#include "Protocol.hh"

namespace Request
{

/**
 * @brief Base class for SDS021 Requests.
 */
class Base : public Protocol
{
public:
  /**
   * @brief Default constructor.
   *
   * @param aDeviceID Indicates which device requests will be addressed to.
   */
  Base( Protocol::DeviceID aDeviceID );

  /**
   * @brief Composes and sends request via SerialPort.
   *
   * @param aPort Serial Port on which SDS021 is accessible.
   */
  virtual void send( SerialPort & aPort ) const = 0;

  /**
   * @brief Helper method that calculates request's checksum
   *
   * @param Request buffer containing request and checksum field
   */
  static void checksum( SerialPort::DataBuffer & aBuffer );
protected:

  Protocol::DeviceID mDeviceID;
};

/// Request Base class shared_ptr type
typedef std::shared_ptr<Base>  Ptr;


enum class Action: Protocol::Byte {
  GET = 0x00,    ///< Indicates that requests reads dust sensor configuration
  SET = 0x01     ///< Indicates that request configures dust sensor
};

enum class SleepMode : Protocol::Byte {
  SLEEP = 0x00,  ///< Sets dust sensor to sleep mode
  WORK  = 0x01   ///< Sets dust sensor to working mode
};

enum class ReportingMode : Protocol::Byte {
  ACTIVE = 0x00, ///< Sets dust sensor to time triggered reporting mode
  QUERY  = 0x01  ///< Sets dust sensor to request triggered reporting mode
};

} // namespace Request
