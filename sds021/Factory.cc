#include <unistd.h>
#include <iostream>
#include <Factory.hh>
#include <Responses/Query.hh>

Factory::Factory()
{
}

void Factory::add( const Response::Ptr & aResponse)
{

  mConsumers[ aResponse->identifier() ] = aResponse;
}

Response::Ptr Factory::receive( SerialPort & aPort )
{
  Response::Ptr lResult;

  while( true )
  {
    while(  Protocol::Head != aPort.ReadByte(0) );

    auto lCommandID( aPort.ReadByte(0) );
    auto lByte1( lCommandID == Response::Query::Id.first
           ? 0x00
           : aPort.ReadByte(0));

    auto lIdentifier( std::make_pair( lCommandID, lByte1 ));
    auto lIter = mConsumers.find( lIdentifier );

    if ( lIter == mConsumers.end() )
      continue;


    if ( lIter != mConsumers.end() )
    {
      lResult = lIter->second;
      lResult->process( aPort );
      break;
    }
  }

  return lResult;
}
