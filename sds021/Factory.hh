#pragma once

#include <map>
#include <utility>

#include <SerialPort.h>
#include <Responses/Base.hh>

/**
 * @brief Factory class for incoming SDS021 responses.
 *
 * This class listens on SerialPort for incoming communication.
 * Traffic is inspected parsed and proper Response object is returned.
 */
class Factory
{
public:
  /*
   * @brief Factory shared_ptr type
   */
  typedef std::shared_ptr< Factory > Ptr;

  /*
   * @brief Consumer container type
   */
  typedef std::map< Response::Base::Identifier, Response::Ptr > Consumers;

  /*
   * @brief Constructs new Factory class
   *
   * Newly created factory does not have any response consumers yet.
   * They need to be added after an instance is created
   */
  Factory();

  /*
   * @brief Adds one or more consumers for incoming messages
   *
   * @param aResponse Consumer that looks for specific message on SerialPort
   */
  void add( const Response::Ptr & aResponse);

  /*
   * @brief Main loop function that parses communication on SerialPort.
   *
   * This is blocking function that exists only after at least one consumer
   * managed to parse incoming communication successfully
   * 
   * @param aPort Serial port on which Factory listens
   * @return Response object
   */
  Response::Ptr receive( SerialPort & aPort );

private:
  Consumers mConsumers;
};
