#pragma once

#include "Base.hh"

namespace Response
{

/**
 * @brief Firmware response message
 */
class Firmware : public Base
{
public:
  static const Identifier Id;

  /**
   * @brief Constructs Firmware response message
   */
  Firmware( );

  /**
   * @brief Processes incoming data stream from SerialPort
   *
   * @param aPort Serial Port on which dust sensor is available
   */
  virtual void process( SerialPort & aPort );

  /**
   * @brief Prints Response message content on stdout
   */
  virtual void print();

private:
  Protocol::Byte mYear;
  Protocol::Byte mMonth;
  Protocol::Byte mDay;
};

} // namespace Response
