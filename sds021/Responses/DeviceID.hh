#pragma once

#include <SerialPort.h>
#include "Base.hh"

namespace Response
{

/**
 * @brief DeviceID response message
 */
class DeviceID : public Base
{
public:
  static const Identifier Id;

  /**
   * @brief Constructs DeviceID response message
   */
  DeviceID( );

  /**
   * @brief Processes incoming data stream from SerialPort
   *
   * @param aPort Serial Port on which dust sensor is available
   */
  virtual void process( SerialPort & aPort );

  /**
   * @brief Prints Response message content on stdout
   */
  virtual void print();

private:
  Protocol::Byte mDeviceId1;
  Protocol::Byte mDeviceId2;
};

} // namespace Response
