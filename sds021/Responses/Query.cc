#include <iostream>
#include <SerialPort.h>
#include <algorithm>

#include "Query.hh"

const Response::Base::Identifier Response::Query::Id( 0xc0, 0x00 );

Response::Query::Query()
  : Base( Id )
  , mPM25(0)
  , mPM10(0)
{
}

void Response::Query::process( SerialPort & aPort )
{
  SerialPort::DataBuffer lBuffer;
  aPort.Read( lBuffer, 8, 0 );

  mPM25     =  ( static_cast<PMLevel>(lBuffer[1]) << 8 ) + static_cast<PMLevel>(lBuffer[0]);
  mPM10     =  ( static_cast<PMLevel>(lBuffer[3]) << 8 ) + static_cast<PMLevel>(lBuffer[2]);
  mDeviceID =  lBuffer[4];
  mDeviceID |= static_cast< Protocol::DeviceID >( lBuffer[5] ) << 8;
  mChecksum =  lBuffer[6];
  mTail     =  lBuffer[7];

}

void Response::Query::print()
{
  std::cout << std::dec
            << "PM2.5: "     << static_cast<float>(mPM25)/10     << " ug/m3"
            << " PM10: "     << static_cast<float>(mPM10)/10     << " ug/m3"
            << " DeviceID: " << std::hex << mDeviceID << std::endl;
}
