#pragma once

#include "Base.hh"

namespace Response
{

/**
 * @brief Query response message
 */
class Query : public Base
{
public:
  typedef uint16_t PMLevel;
  static const Identifier Id;

  /**
   * @brief Constructs Query response message
   */
  Query( );

  /**
   * @brief Processes incoming data stream from SerialPort
   *
   * @param aPort Serial Port on which dust sensor is available
   */
  virtual void process( SerialPort & aPort );

  /**
   * @brief Prints Response message content on stdout
   */
  virtual void print();
private:

  PMLevel mPM25;
  PMLevel mPM10;
};

} // namespace Response
