#pragma once

#include <vector>
#include <cstdint>
#include <memory>
#include <SerialPort.h>

#include "Protocol.hh"

namespace Response
{

/**
 * @brief Base class for SDS021 Responses.
 */
class Base : public Protocol
{
public:
  typedef std::pair<Byte,Byte>  Identifier;

  /**
   * @brief Default constructor.
   *
   * @param aIdentifer Represents 3rd byte in protocol definition.
   *                   Used by factory while looking for message consumers
   */
  Base( const Identifier & aIdentifier);

  /**
   * @brief Gets response identifier.
   *
   * @return Response identifier
   */
  Identifier identifier() const;

  /**
   * @brief Processes incoming data stream from SerialPort
   *
   * @param aPort Serial Port on which dust sensor is available
   */
  virtual void process( SerialPort & aPort ) = 0;

  /**
   * @brief Prints Response message content on stdout
   */
  virtual void print() = 0;

protected:
  Protocol::DeviceID mDeviceID;
  Protocol::Byte     mChecksum;
  Protocol::Byte     mTail;

private:
  Identifier         mIdentifier;
};

typedef std::shared_ptr<Base> Ptr;

} // namespace Response
