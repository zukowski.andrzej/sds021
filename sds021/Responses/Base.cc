#include <istream>
#include "Base.hh"

Response::Base::Base( const Identifier & aIdentifier)
  : mIdentifier( aIdentifier )
  , mDeviceID(0)
  , mChecksum(0)
  , mTail(0)
{
}

Response::Base::Identifier Response::Base::identifier() const
{
  return mIdentifier;
}
