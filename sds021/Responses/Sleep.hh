#pragma once

#include "Base.hh"

namespace Response
{

/**
 * @brief Sleep Mode response message
 */
class Sleep : public Base
{
public:
  static const Identifier Id;

  /**
   * @brief Constructs Sleep response message
   */
  Sleep( );

  /**
   * @brief Processes incoming data stream from SerialPort
   *
   * @param aPort Serial Port on which dust sensor is available
   */
  virtual void process( SerialPort & aPort );

  /**
   * @brief Prints Response message content on stdout
   */
  virtual void print();

private:
  Protocol::Byte mQuery;
  Protocol::Byte mMode;
};

} // namespace Response
