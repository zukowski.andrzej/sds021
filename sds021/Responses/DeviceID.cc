#include <iostream>
#include "DeviceID.hh"

const Response::Base::Identifier Response::DeviceID::Id( 0xc5, 0x05 );

Response::DeviceID::DeviceID()
  : Base( Id )
  , mDeviceId1(0)
  , mDeviceId2(0)
{
}

void Response::DeviceID::process( SerialPort & aPort )
{
  SerialPort::DataBuffer lBuffer;
  aPort.Read( lBuffer, 7, 0 );

  mDeviceId1 = lBuffer[3];
  mDeviceId2 = lBuffer[4];
  mChecksum  = lBuffer[5];
  mTail      = lBuffer[6];
}

void Response::DeviceID::print()
{
  std::cout << std::dec
            << "SET mode: "
            << " DeviceID: " << std::hex
            << static_cast<int32_t>(mDeviceId1)
            << static_cast<int32_t>(mDeviceId2) << std::endl;
}
