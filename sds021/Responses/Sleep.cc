#include <iostream>
#include <SerialPort.h>
#include "Sleep.hh"

const Response::Base::Identifier Response::Sleep::Id( 0xc5, 0x06 );

Response::Sleep::Sleep()
  : Base( Id )
  , mQuery(0)
  , mMode(0)
{
}

void Response::Sleep::process( SerialPort & aPort )
{
  SerialPort::DataBuffer lBuffer;
  aPort.Read( lBuffer, 7, 0 );

  mQuery     =  lBuffer[0];
  mMode      =  lBuffer[1];
  mDeviceID  =  lBuffer[3];
  mDeviceID  |= static_cast< Protocol::DeviceID >( lBuffer[4] ) << 8;
  mChecksum  =  lBuffer[5];
  mTail      =  lBuffer[6];
}

void Response::Sleep::print()
{
  std::string lQuery( mQuery ? "SET mode: " : "GET mode: " );
  std::string lMode(  mMode  ? "Sleep"      : "Work" );

  std::cout << std::dec
            << lQuery << lMode
            << " DeviceID: " << std::hex << mDeviceID << std::endl;
}
