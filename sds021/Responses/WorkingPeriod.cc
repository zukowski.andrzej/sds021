#include <iostream>
#include <SerialPort.h>
#include "WorkingPeriod.hh"

const Response::Base::Identifier Response::WorkingPeriod::Id( 0xc5, 0x08 );

Response::WorkingPeriod::WorkingPeriod()
  : Base( Id )
  , mQuery(0)
  , mPeriod(0)
{
}

void Response::WorkingPeriod::process( SerialPort & aPort )
{
  SerialPort::DataBuffer lBuffer;
  aPort.Read( lBuffer, 7, 0 );

  mQuery     =  lBuffer[0];
  mPeriod    =  lBuffer[1];
  mDeviceID  =  lBuffer[3];
  mDeviceID  |= static_cast< Protocol::DeviceID >( lBuffer[4] ) << 8;
  mChecksum  =  lBuffer[5];
  mTail      =  lBuffer[6];
}

void Response::WorkingPeriod::print()
{
  std::string lQuery( mQuery ? "SET mode: " : "GET mode: " );
  std::string lPeriod;

  if( !mPeriod )
  {
    lPeriod = "Continuous";
  }
  else
  {
    lPeriod += static_cast<uint32_t>( mPeriod);
    lPeriod += " minutes";
  }

  std::cout << std::dec
            << lQuery << lPeriod
            << " DeviceID: " << std::hex << mDeviceID << std::endl;
}
