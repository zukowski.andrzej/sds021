#include <iostream>
#include <SerialPort.h>
#include "Firmware.hh"

const Response::Base::Identifier Response::Firmware::Id( 0xc5, 0x07 );

Response::Firmware::Firmware()
  : Base( Id )
  , mYear(0)
  , mMonth(0)
  , mDay(0)
{
}

void Response::Firmware::process( SerialPort & aPort )
{
  SerialPort::DataBuffer lBuffer;
  aPort.Read( lBuffer, 7, 0 );

  mYear      =  lBuffer[0];
  mMonth     =  lBuffer[1];
  mDay       =  lBuffer[2];
  mDeviceID  =  lBuffer[3];
  mDeviceID  |= static_cast< Protocol::DeviceID >( lBuffer[4] ) << 8;
  mChecksum  =  lBuffer[5];
  mTail      =  lBuffer[6];
}

void Response::Firmware::print()
{
  std::cout << std::dec<< std::dec
            << "Firmware: "  << static_cast<int32_t>(mYear)
            << "/"           << static_cast<int32_t>(mMonth)
            << "/"           << static_cast<int32_t>(mDay)
            << " DeviceID: " << std::hex << mDeviceID << std::endl;
}
