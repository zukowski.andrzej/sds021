#pragma once

#include "Base.hh"

namespace Response
{

/**
 * @brief Working Period response message
 */
class WorkingPeriod : public Base
{
public:
  static const Identifier Id;

  /**
   * @brief Constructs Working Period response message
   */
  WorkingPeriod( );

  /**
   * @brief Processes incoming data stream from SerialPort
   *
   * @param aPort Serial Port on which dust sensor is available
   */
  virtual void process( SerialPort & aPort );

  /**
   * @brief Prints Response message content on stdout
   */
  virtual void print();

private:
  Protocol::Byte mQuery;
  Protocol::Byte mPeriod;
};

} // namespace Response
