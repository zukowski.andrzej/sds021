#include <iostream>
#include <SerialPort.h>
#include "Reporting.hh"

const Response::Base::Identifier Response::Reporting::Id( 0xc5, 0x02 );

Response::Reporting::Reporting()
  : Base( Id )
  , mQuery(0)
  , mMode(0)
{
}

void Response::Reporting::process( SerialPort & aPort )
{
  SerialPort::DataBuffer lBuffer;
  aPort.Read( lBuffer, 7, 0 );

  mQuery     =  lBuffer[0];
  mMode      =  lBuffer[1];
  mDeviceID  =  lBuffer[3];
  mDeviceID  |= static_cast< Protocol::DeviceID >( lBuffer[4] ) << 8;
  mChecksum  =  lBuffer[5];
  mTail      =  lBuffer[6];
}

void Response::Reporting::print()
{
  std::string lQuery( mQuery ? "SET mode: " : "GET mode: " );
  std::string lMode(  mMode  ? "Query"      : "Active" );

  std::cout << std::dec
            << lQuery << lMode
            << " DeviceID: " << std::hex << mDeviceID << std::endl;
}
