#pragma once
#include <memory>

/**
 * @brief Base class for SDS021 protocol definition
 */
class Protocol
{
public:
  /// Byte type
  typedef uint8_t                     Byte;

  /// Serial port device type
  typedef std::string                 Device;

  /// 2 Byte device identifier
  typedef uint16_t                    DeviceID;

  /// Device shared_ptr type
  typedef std::shared_ptr< Device >   DevicePtr;

  /// Device identifier shared_ptr
  typedef std::shared_ptr< DeviceID > DeviceIDPtr;

  static const Byte                   Head;
  static const Byte                   Tail;

};
